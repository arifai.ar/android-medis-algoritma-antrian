package com.anha.medis.api.model;

import com.google.gson.annotations.SerializedName;

public class DokterResponse {
    @SerializedName("id")
    private String id;
    @SerializedName("hari")
    private String hari;
    @SerializedName("jam_buka")
    private String jamBuka;
    @SerializedName("jam_tutup")
    private String jamTutup;
    @SerializedName("dokter")
    private String dokter;
    @SerializedName("kuota")
    private String kuota;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getJamBuka() {
        return jamBuka;
    }

    public void setJamBuka(String jamBuka) {
        this.jamBuka = jamBuka;
    }

    public String getJamTutup() {
        return jamTutup;
    }

    public void setJamTutup(String jamTutup) {
        this.jamTutup = jamTutup;
    }

    public String getDokter() {
        return dokter;
    }

    public void setDokter(String dokter) {
        this.dokter = dokter;
    }

    public String getKuota() {
        return kuota;
    }

    public void setKuota(String kuota) {
        this.kuota = kuota;
    }
}
