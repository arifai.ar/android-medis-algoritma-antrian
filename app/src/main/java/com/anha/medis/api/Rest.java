package com.anha.medis.api;

import com.anha.medis.api.model.BaseResponse;
import com.anha.medis.api.model.DokterResponse;
import com.anha.medis.api.model.NullResponse;
import com.anha.medis.api.model.RiwayatAntrianResponse;

import java.util.List;

import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Rest {
    @FormUrlEncoded
    @POST("pasien")
    Observable<NullResponse> createPasien(
            @Field("nama") String nama,
            @Field("umur") String umur,
            @Field("jenis_kelamin") String jen_kel,
            @Field("no_hp") String no_hp,
            @Field("alamat") String alamat,
            @Field("agama") String agama,
            @Field("penjamin") String penjamin,
            @Field("tanggal_kunjungan") String tanggal_kunjungan,
            @Field("status") String status
    );

    @GET("dokter")
    Observable<BaseResponse<List<DokterResponse>>> getDokter();

    @GET("riwayat-antrian")
    Observable<BaseResponse<List<RiwayatAntrianResponse>>> getRiwayatAntrian();
}
