package com.anha.medis.utils;

import android.util.Log;

import com.anha.medis.configs.Config;

public class LogUtils {
    public static void debug(String tag, String mess){
        if(mess == null){
            Log.d("NULL COY", "NULL COY");
        }
        if(Config.DEBUGABLE) Log.d(tag, mess);
    }
}
