package com.anha.medis.utils;

import android.content.Context;
import android.widget.Toast;

public class ToastUtils {
    public static void shortShow(Context c, String m){
        Toast.makeText(c, m, Toast.LENGTH_SHORT).show();
    }

    public static void longShow(Context c, String m){
        Toast.makeText(c, m, Toast.LENGTH_LONG).show();
    }
}
