/*
 * Copyright (c) 2019.
 * author Ahmad Rifa'i
 */

package com.anha.medis.utils;
import android.content.Context;
import android.content.SharedPreferences;

import com.anha.medis.configs.Config;


/**
 * Created by LENOVO on 16/03/2018.
 */

public class PreferencesUtils {
    public static boolean getExistPref(Context c, String k){
        return c.getSharedPreferences(Config.ID_PREFERENCES,Context.MODE_PRIVATE).contains(k);
    }

    public static void setPref(Context c, String k, String v){
        c.getSharedPreferences(Config.ID_PREFERENCES,Context.MODE_PRIVATE).edit().putString(k,v).commit();
    }
    public static String getPref(Context c, String k){
        return c.getSharedPreferences(Config.ID_PREFERENCES,Context.MODE_PRIVATE).getString(k,null);
    }

    public static SharedPreferences get(Context c){
        return c.getSharedPreferences(Config.ID_PREFERENCES, Context.MODE_PRIVATE);
    }

    public static void clear(Context context){
        get(context).edit().clear().apply();
    }
}
