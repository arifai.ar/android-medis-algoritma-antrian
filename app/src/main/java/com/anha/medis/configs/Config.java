package com.anha.medis.configs;

public class Config {

    public static final boolean DEBUGABLE = false;
    public static final boolean DEVELOPMENT = false;

    private static final String LOCAL_IP = "http://192.168.0.104/medis";
    private static final String ONLINE_IP = "https://medis.deerdeveloper.com";
//    private static final String ONLINE_IP = "https://mediss.000webhostapp.com";
//    private static final String ONLINE_IP = "https://12b5171f.ngrok.io/medis";

    public static final String BASE_URL = (DEVELOPMENT ? LOCAL_IP : ONLINE_IP);
    public static final String API_URL = BASE_URL+"/api/";

    public static final String ID_PREFERENCES = "medis_preferences_deerdev";
    public static final String SESSION_NAMA = "session_nama";
    public static final String SESSION_WT = "session_wt";
    public static final String SESSION_PRIORITAS = "session_prioritas";
    public static final String SESSION_NOMOR = "session_nomor";
    public static final String SESSION_TANGGAL = "session_tanggal";
    public static final String SESSION_JAM_BUKA = "session_jam_buka";

//    public static final String JAM_BUKA = "12:00:00";
}