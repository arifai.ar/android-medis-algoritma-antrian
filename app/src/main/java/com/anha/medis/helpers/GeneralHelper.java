package com.anha.medis.helpers;

import com.anha.medis.configs.Config;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class GeneralHelper {
    private static final String DATE_FORMAT = "HH:mm:ss";

    public static Date getDateBySet(int h, int m, int s){
        Date date = new Date();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, h);// for 6 hour
        calendar.set(Calendar.MINUTE, m);// for 0 min
        calendar.set(Calendar.SECOND, s);// for 0 sec
        System.out.println(calendar.getTime());// print 'Mon Mar 28 06:00:00 ALMT 2016'
        return calendar.getTime();
    }

    public static String getTimeByAdd(String jamBuka, int minute){
        String myTime = jamBuka;
        SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
        Date d = null;
        try {
            d = df.parse(myTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(d);
        cal.add(Calendar.MINUTE, minute);
        String newTime = df.format(cal.getTime());
        return newTime;
    }

    public static String getTimeSubstract(String d1, String d2){
        java.text.DateFormat df = new java.text.SimpleDateFormat(DATE_FORMAT);
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = df.parse(d1);
            date2 = df.parse(d2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long diff = date2.getTime() - date1.getTime();
        System.out.println(diff);
        if(diff > 0){
            return getDate(diff);
        } else {
            return getDate(0);
        }
    }

    private static String getDate(long diff) {
        int timeInSeconds = (int) diff / 1000;
        int hours, minutes, seconds;
        hours = timeInSeconds / 3600;
        timeInSeconds = timeInSeconds - (hours * 3600);
        minutes = timeInSeconds / 60;
        timeInSeconds = timeInSeconds - (minutes * 60);
        seconds = timeInSeconds;
        String diffTime = (hours<10 ? "0" + hours : hours) + ":" + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
        return diffTime;
    }
}
