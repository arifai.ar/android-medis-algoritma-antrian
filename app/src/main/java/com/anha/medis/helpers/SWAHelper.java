package com.anha.medis.helpers;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Handler;

import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;


public class SWAHelper {
    private Context context;
    private SweetAlertDialog swa;

    public SWAHelper(Context context) {
        this.context = context;
    }

    public void showLoading(DialogInterface.OnCancelListener listener){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.PROGRESS_TYPE);
        //swa.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
        swa.getProgressHelper().setBarColor(Color.parseColor("#9FC41A"));
        swa.setTitleText("Loading");
        swa.setCancelable(true);
        swa.setOnCancelListener(listener);
        swa.show();
    }

    public SweetAlertDialog showLoading(){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.PROGRESS_TYPE);
        //swa.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
        swa.getProgressHelper().setBarColor(Color.parseColor("#9FC41A"));
        swa.setTitleText("Loading");
        swa.setCancelable(false);
        swa.show();
        return swa;
    }

    public void dismiss(){
        swa.dismiss();
        //swa.hide();
    }

    public void dismiss(int wait){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
               swa.dismiss();
            }
        }, wait);
    }

    public void info(String title, String text){
        swa = new SweetAlertDialog(this.context);
        //swa.changeAlertType(SweetAlertDialog.NORMAL_TYPE);
        swa.setTitleText(title).setContentText(text).show();
    }

    public void info(String title, String text, SweetAlertDialog.OnSweetClickListener listener){
        swa = new SweetAlertDialog(this.context);
        //swa.changeAlertType(SweetAlertDialog.NORMAL_TYPE);
        swa.setTitleText(title).setContentText(text).show();
        swa.setCancelText("Tidak");
        swa.setConfirmText("Ya");
        swa.setConfirmClickListener(listener);
    }

    public void success(){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.SUCCESS_TYPE);
        //swa.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        swa.setTitleText("Sukses")
                .show();
    }

    public void success(SweetAlertDialog.OnSweetClickListener listener){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.SUCCESS_TYPE);
        //swa.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        swa.setTitleText("Sukses")
                .setConfirmClickListener(listener)
                .show();
    }

    public void success(boolean b){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.SUCCESS_TYPE);
        //swa.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        swa.setTitleText("Sukses")
                .showCancelButton(!b)
                .showContentText(!b)
                .setConfirmText(".......")
                .show();
    }


    public void error(){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.ERROR_TYPE);
        //swa.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        swa.setTitleText("Oops...")
                .show();
    }

    public void warning(){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.WARNING_TYPE);
        //swa.changeAlertType(SweetAlertDialog.WARNING_TYPE);
        swa.setTitleText("Peringatan")
                .show();
    }

    public void successMessage(String text){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.SUCCESS_TYPE);
        //swa.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        swa.setTitleText("Sukses")
                .setContentText(text)
                .show();
    }

    public void successMessage(String title, String text, SweetAlertDialog.OnSweetClickListener s){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.SUCCESS_TYPE);
        //swa.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
        swa.setTitleText(title)
                .setContentText(text)
                .show();
        swa.setConfirmClickListener(s);
    }

    public void errorMessage(String text){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.ERROR_TYPE);
        //swa.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        swa.setTitleText("Maaf")
                .setContentText(text)
                .show();
    }

    public void errorMessage(String text, SweetAlertDialog.OnSweetClickListener s){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.ERROR_TYPE);
        //swa.changeAlertType(SweetAlertDialog.ERROR_TYPE);
        swa.setTitleText("Maaf")
                .setContentText(text)
                .setConfirmClickListener(s)
                .show();
    }

    public void warningMessage(String title, String text, String confirm){
        swa = new SweetAlertDialog(this.context, SweetAlertDialog.WARNING_TYPE);
        //swa.changeAlertType(SweetAlertDialog.WARNING_TYPE);
        swa.setTitleText(title)
                .setContentText(text)
                .setConfirmText(confirm)
                .show();
    }
}
