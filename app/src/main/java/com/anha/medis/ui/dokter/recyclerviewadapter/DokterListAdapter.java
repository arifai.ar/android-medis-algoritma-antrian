package com.anha.medis.ui.dokter.recyclerviewadapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.anha.medis.R;

import java.util.List;

public class DokterListAdapter extends RecyclerView.Adapter<DokterListAdapter.ViewHolder> {

    private List<DokterListModel> list;

    public DokterListAdapter(List<DokterListModel> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_item_dokter, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        holder.dokter.setText(list.get(i).getDokter());
        holder.hari.setText("Hari \t\t\t\t\t: "+list.get(i).getHari());
        holder.jam.setText("Jam Buka \t: "+list.get(i).getJam_buka()+" s.d. "+list.get(i).getJam_tutup());
        holder.kuota.setText("Kuota \t\t\t\t: "+list.get(i).getKuota());
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView dokter, hari, jam, kuota;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            dokter = itemView.findViewById(R.id.dokter);
            hari = itemView.findViewById(R.id.hari);
            jam = itemView.findViewById(R.id.jam);
            kuota = itemView.findViewById(R.id.kuota);
        }
    }
}
