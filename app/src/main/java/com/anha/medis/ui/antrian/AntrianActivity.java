package com.anha.medis.ui.antrian;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.MenuItem;
import android.widget.TextView;

import com.anha.medis.R;
import com.anha.medis.configs.Config;
import com.anha.medis.helpers.GeneralHelper;
import com.anha.medis.ui.riwayat_antrian.RiwayatAntrian;
import com.anha.medis.utils.PreferencesUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

public class AntrianActivity extends AppCompatActivity {

    private TextView nama, tanggal, nomorAntrian, nomorPrioritas, waktuTunggu;

    private final int UPDATE_TIME = 0x01;
    private SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss");
    private Thread timeThread;

    private String wtSession;
    private boolean threadFinish = false;

    private Runnable runnableTime = new Runnable() {
        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted() && !threadFinish) {
                handler.obtainMessage(UPDATE_TIME, format.format(Calendar.getInstance().getTime())).sendToTarget();
                try {
                    timeThread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if(msg.what == UPDATE_TIME) {
                String s = GeneralHelper.getTimeSubstract(msg.obj.toString(), wtSession);
                waktuTunggu.setText(s);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_antrian);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initView();
    }

    private void initView(){
        nama = findViewById(R.id.nama);
        tanggal = findViewById(R.id.tanggal);
        nomorAntrian = findViewById(R.id.nomor);
        nomorPrioritas = findViewById(R.id.prioritas);
        waktuTunggu = findViewById(R.id.wt);

        if(PreferencesUtils.getExistPref(this, Config.SESSION_NOMOR)
                && !PreferencesUtils.getPref(this, Config.SESSION_NOMOR).equalsIgnoreCase("")){

            nama.setText(PreferencesUtils.getPref(this, Config.SESSION_NAMA));
            tanggal.setText(PreferencesUtils.getPref(this, Config.SESSION_TANGGAL));
            nomorAntrian.setText(PreferencesUtils.getPref(this, Config.SESSION_PRIORITAS));
            nomorPrioritas.setText("Nomor urut : "+PreferencesUtils.getPref(this, Config.SESSION_NOMOR));

            wtSession = GeneralHelper.getTimeByAdd(PreferencesUtils.getPref(this, Config.SESSION_JAM_BUKA), Integer.valueOf(PreferencesUtils.getPref(this, Config.SESSION_WT)));

            timeThread = new Thread(runnableTime);
            timeThread.start();
        } else {
            startActivity(new Intent(this, RiwayatAntrian.class));
            this.finish();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home :
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        threadFinish = true;
    }
}
