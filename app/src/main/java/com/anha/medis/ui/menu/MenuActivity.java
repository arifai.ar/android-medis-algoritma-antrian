package com.anha.medis.ui.menu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import com.anha.medis.R;
import com.anha.medis.ui.antrian.AntrianActivity;
import com.anha.medis.ui.dokter.DokterActivity;
import com.anha.medis.ui.maps.MapsActivity;
import com.anha.medis.ui.menu.gridadapter.MenuGridAdapter;
import com.anha.medis.ui.menu.gridadapter.MenuGridModel;
import com.anha.medis.ui.pendaftaran.PendaftaranActivity;
import com.anha.medis.ui.petunjuk.PetunjukActivity;
import com.anha.medis.ui.riwayat_antrian.RiwayatAntrian;
import com.anha.medis.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MenuActivity extends AppCompatActivity {

    private GridView gridView;
    private List<MenuGridModel> gridModelList = new ArrayList<>();
    private MenuGridAdapter menuGridAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        initView();
    }

    private void initView(){
        gridView = findViewById(R.id.menu_grid);
        gridModelList.add(new MenuGridModel("Pendaftaran", R.drawable.ic_group_add));
        gridModelList.add(new MenuGridModel("Dokter", R.drawable.ic_person_outline));
        gridModelList.add(new MenuGridModel("Riwayat Antrian", R.drawable.ic_view_list_black_24dp));
        gridModelList.add(new MenuGridModel("Lihat Antrian", R.drawable.ic_featured_play_list));
        gridModelList.add(new MenuGridModel("Maps", R.drawable.ic_map));
        gridModelList.add(new MenuGridModel("Petunjuk", R.drawable.ic_help));
        menuGridAdapter = new MenuGridAdapter(this, gridModelList);
        gridView.setAdapter(menuGridAdapter);

        /**
         * Proble gridview item click listener
         * link : https://stackoverflow.com/questions/20052631/android-gridview-with-custom-baseadapter-create-onclicklistener
         */
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                itemGridViewClick(i);
            }
        });
    }

    private void itemGridViewClick(int pos){
        ToastUtils.shortShow(this, gridModelList.get(pos).getTitle());
        switch (pos){
            case 0 :
                startActivity(new Intent(this, PendaftaranActivity.class));
                break;
            case 1 :
                startActivity(new Intent(this, DokterActivity.class));
                break;
            case 2 :
                startActivity(new Intent(this, RiwayatAntrian.class));
                break;
            case 3 :
                startActivity(new Intent(this, AntrianActivity.class));
                break;
            case 4 :
                startActivity(new Intent(this, MapsActivity.class));
                break;
            case 5 :
                startActivity(new Intent(this, PetunjukActivity.class));
                break;
        }
    }
}
