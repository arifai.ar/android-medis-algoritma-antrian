package com.anha.medis.ui.pendaftaran;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.anha.medis.R;
import com.anha.medis.api.Rest;
import com.anha.medis.api.model.NullResponse;
import com.anha.medis.configs.Config;
import com.anha.medis.configs.Messages;
import com.anha.medis.helpers.SWAHelper;
import com.anha.medis.utils.PreferencesUtils;
import com.anha.medis.utils.RetrofitClient;
import com.ontbee.legacyforks.cn.pedant.SweetAlert.SweetAlertDialog;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PendaftaranActivity extends AppCompatActivity {

    private Button btnDaftar;
    private EditText etNamaPasien, etUmur, etJenisKelamin, etNoHP, etAlamat, etPenjamin, etTanggalKunjungan;
    private AppCompatSpinner spAgama, spStatus;

    private SWAHelper alert;

    private Calendar calendar = Calendar.getInstance();
    private SimpleDateFormat format = new SimpleDateFormat("d MMMM yyyy");
    private SimpleDateFormat formatTo = new SimpleDateFormat("yyyy-MM-d");
    private DatePickerDialog pickerDialog;

    private DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            etTanggalKunjungan.setText(format.format(calendar.getTime()));
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pendaftaran);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        alert = new SWAHelper(this);

        btnDaftar = findViewById(R.id.daftar);
        etNamaPasien = findViewById(R.id.nama);
        etUmur = findViewById(R.id.umur);
        etJenisKelamin = findViewById(R.id.jenis_kelamin);
        etNoHP = findViewById(R.id.nohp);
        etAlamat = findViewById(R.id.alamat);
        etPenjamin = findViewById(R.id.penjamin);
        etTanggalKunjungan = findViewById(R.id.tanggal);
        spStatus = findViewById(R.id.status);
        spAgama = findViewById(R.id.agama);

        spAgama.setAdapter(new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, new String[]{"Islam", "Kristen", "Hindu", "Budha"}));
        spStatus.setAdapter(new ArrayAdapter<String>(this, R.layout.support_simple_spinner_dropdown_item, new String[]{"Kronis", "Sedang", "Tidak kronis"}));

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alert.info("Apakah anda yakin ?", "Data pasien akan dimasukan", new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.dismiss();
                        onDaftarPasien();
                    }
                });
            }
        });

        pickerDialog =  new DatePickerDialog(this, date, calendar
                .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));

        etTanggalKunjungan.setText(format.format(calendar.getTime()));
        etTanggalKunjungan.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                pickerDialog.show();
                return false;
            }
        });

        btnDaftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDaftarPasien();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home :
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void onDaftarPasien(){
        // check input
        if(etNamaPasien.getText().toString().isEmpty()
        || etUmur.getText().toString().isEmpty()
        || etJenisKelamin.getText().toString().isEmpty()
        || etAlamat.getText().toString().isEmpty()
        || etPenjamin.getText().toString().isEmpty()){
            alert.errorMessage("Data harus lengkap");
            return;
        }

        // call api for register pasien
        alert.showLoading();
        String tgl = "";
        try {
            tgl = formatTo.format(format.parse(etTanggalKunjungan.getText().toString()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        RetrofitClient
                .APIService(Config.API_URL)
                .create(Rest.class)
                .createPasien(
                        etNamaPasien.getText().toString(),
                        etUmur.getText().toString(),
                        etJenisKelamin.getText().toString(),
                        etNoHP.getText().toString(),
                        etAlamat.getText().toString(),
                        spAgama.getSelectedItem().toString(),
                        etPenjamin.getText().toString(),
                        tgl,
                        spStatus.getSelectedItem().toString())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<NullResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(NullResponse nullResponse) {
                        alert.dismiss();
                        if(nullResponse.isSuccess()){
                            alert.success(new SweetAlertDialog.OnSweetClickListener() {
                                @Override
                                public void onClick(SweetAlertDialog sweetAlertDialog) {
                                    PreferencesUtils.setPref(PendaftaranActivity.this, Config.SESSION_NAMA, etNamaPasien.getText().toString());
                                    sweetAlertDialog.dismiss();
                                    PendaftaranActivity.this.finish();
                                }
                            });
                        } else {
                            alert.errorMessage(nullResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        alert.dismiss();
                        alert.errorMessage(Messages.ERROR_API);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}