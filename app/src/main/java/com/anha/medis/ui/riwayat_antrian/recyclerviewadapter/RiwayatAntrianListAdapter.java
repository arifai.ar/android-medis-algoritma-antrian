package com.anha.medis.ui.riwayat_antrian.recyclerviewadapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.anha.medis.R;
import com.anha.medis.configs.Config;
import com.anha.medis.helpers.GeneralHelper;

import java.util.List;

public class RiwayatAntrianListAdapter extends RecyclerView.Adapter<RiwayatAntrianListAdapter.ViewHolder> {

    private List<RiwayatAntrianListModel> list;
    private Listener listener;

    public RiwayatAntrianListAdapter(List<RiwayatAntrianListModel> list, Listener listener) {
        this.list = list;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_list_item_riwayat_antrian, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int i) {
        holder.nomor.setText(list.get(i).getNomer());
        holder.nama.setText(list.get(i).getNama());
        holder.bt.setText(list.get(i).getBT()+" menit");
        holder.at.setText(list.get(i).getAT()+" menit");
        holder.tanggal.setText(list.get(i).getTanggal());
        holder.wt.setText(GeneralHelper.getTimeByAdd(list.get(i).getJam_buka(), Integer.valueOf(list.get(i).getWT())));
        holder.ta.setText(list.get(i).getTA()+" menit");
        holder.root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickItem(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nomor, nama, bt, at, tanggal, wt, ta;
        private CardView root;
        ViewHolder(@NonNull View itemView) {
            super(itemView);
            nomor = itemView.findViewById(R.id.nomor);
            nama = itemView.findViewById(R.id.nama);
            bt = itemView.findViewById(R.id.bt);
            at = itemView.findViewById(R.id.at);
            tanggal = itemView.findViewById(R.id.tanggal);
            wt = itemView.findViewById(R.id.wt);
            ta = itemView.findViewById(R.id.ta);
            root = itemView.findViewById(R.id.root);
        }
    }

    public interface Listener {
        void onClickItem(int i);
    }
}
