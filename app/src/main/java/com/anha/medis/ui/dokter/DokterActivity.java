package com.anha.medis.ui.dokter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.MenuItem;

import com.anha.medis.R;
import com.anha.medis.api.Rest;
import com.anha.medis.api.model.BaseResponse;
import com.anha.medis.api.model.DokterResponse;
import com.anha.medis.configs.Config;
import com.anha.medis.configs.Messages;
import com.anha.medis.helpers.SWAHelper;
import com.anha.medis.ui.dokter.recyclerviewadapter.DokterListAdapter;
import com.anha.medis.ui.dokter.recyclerviewadapter.DokterListModel;
import com.anha.medis.utils.RetrofitClient;
import com.anha.medis.utils.ToastUtils;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class DokterActivity extends AppCompatActivity {

    private List<DokterListModel> dokterListModels = new ArrayList<>();
    private DokterListAdapter dokterListAdapter;
    private RecyclerView recyclerView;

    private SWAHelper alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dokter);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        alert = new SWAHelper(this);
        initView();
    }

    private void initView(){
        ToastUtils.shortShow(this, "Render list view");
        recyclerView = findViewById(R.id.recyclerview);

//        dokterListModels.add(new DokterListModel("Dr. Budiman", "Senin", "19.00", "24.00", "10"));
//        dokterListModels.add(new DokterListModel("Dr. Surya", "Selasa", "19.00", "24.00", "10"));
//        dokterListModels.add(new DokterListModel("Dr. Nike", "Kamis", "19.00", "24.00", "10"));
//        dokterListModels.add(new DokterListModel("Dr. Boike", "Jumat", "19.00", "24.00", "10"));
//        dokterListModels.add(new DokterListModel("Dr. Jono", "Selasa", "19.00", "24.00", "10"));
//        dokterListModels.add(new DokterListModel("Dr. Suhasono", "Rabu", "19.00", "24.00", "10"));
//        dokterListModels.add(new DokterListModel("Dr. Sutristo", "Sabtu", "19.00", "24.00", "10"));

        dokterListAdapter = new DokterListAdapter(dokterListModels);
        recyclerView.setAdapter(dokterListAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        getData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home :
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void getData(){
        alert.showLoading();
        RetrofitClient.APIService(Config.API_URL)
                .create(Rest.class)
                .getDokter()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse<List<DokterResponse>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BaseResponse<List<DokterResponse>> datas) {
                        alert.dismiss();
                        if(datas.isSuccess()) {
                            for (DokterResponse d : datas.getData()) {
                                dokterListModels.add(new DokterListModel(
                                        d.getDokter(),
                                        d.getHari(),
                                        d.getJamBuka(),
                                        d.getJamTutup(),
                                        d.getKuota()
                                ));
                            }
                            dokterListAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        alert.dismiss();
                        alert.errorMessage(Messages.ERROR_API);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }
}
