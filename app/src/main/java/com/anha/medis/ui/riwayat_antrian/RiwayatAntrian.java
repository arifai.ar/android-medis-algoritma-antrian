package com.anha.medis.ui.riwayat_antrian;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.anha.medis.R;
import com.anha.medis.api.Rest;
import com.anha.medis.api.model.BaseResponse;
import com.anha.medis.api.model.DokterResponse;
import com.anha.medis.api.model.RiwayatAntrianResponse;
import com.anha.medis.configs.Config;
import com.anha.medis.helpers.SWAHelper;
import com.anha.medis.ui.antrian.AntrianActivity;
import com.anha.medis.ui.riwayat_antrian.recyclerviewadapter.RiwayatAntrianListAdapter;
import com.anha.medis.ui.riwayat_antrian.recyclerviewadapter.RiwayatAntrianListModel;
import com.anha.medis.utils.PreferencesUtils;
import com.anha.medis.utils.RetrofitClient;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class RiwayatAntrian extends AppCompatActivity implements RiwayatAntrianListAdapter.Listener {

    private List<RiwayatAntrianListModel> riwayatAntrianListModels = new ArrayList<>();
    private RiwayatAntrianListAdapter antrianListAdapter;
    private RecyclerView recyclerView;

    private SWAHelper alert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riwayat_antrian);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        alert = new SWAHelper(this);
        initView();
    }

    private void initView(){
        recyclerView = findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        antrianListAdapter = new RiwayatAntrianListAdapter(riwayatAntrianListModels, this);
        recyclerView.setAdapter(antrianListAdapter);
        getData();
    }

    private void getData(){
        alert.showLoading();
        RetrofitClient.APIService(Config.API_URL).create(Rest.class)
                .getRiwayatAntrian()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<BaseResponse<List<RiwayatAntrianResponse>>>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(BaseResponse<List<RiwayatAntrianResponse>> listBaseResponse) {
                        alert.dismiss();
                        if(listBaseResponse.isSuccess()){
                            for(RiwayatAntrianResponse r : listBaseResponse.getData()){
                                riwayatAntrianListModels.add(new RiwayatAntrianListModel(
                                        r.getNomor(),
                                        r.getNomor(),
                                        r.getNama(),
                                        r.getBt(),
                                        r.getAt(),
                                        r.getTanggal(),
                                        r.getRt(),
                                        r.getTa(),
                                        r.getWt(),
                                        r.getJam_buka()
                                ));
                            }
                            antrianListAdapter.notifyDataSetChanged();
                        } else {
                            alert.errorMessage(listBaseResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        alert.dismiss();
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home :
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClickItem(int i) {
        PreferencesUtils.setPref(this, Config.SESSION_NAMA, riwayatAntrianListModels.get(i).getNama());
        PreferencesUtils.setPref(this, Config.SESSION_WT, riwayatAntrianListModels.get(i).getWT());
        PreferencesUtils.setPref(this, Config.SESSION_TANGGAL, riwayatAntrianListModels.get(i).getTanggal());
        PreferencesUtils.setPref(this, Config.SESSION_NOMOR, riwayatAntrianListModels.get(i).getNomer());
        PreferencesUtils.setPref(this, Config.SESSION_PRIORITAS, String.valueOf(i+1));
        PreferencesUtils.setPref(this, Config.SESSION_JAM_BUKA, riwayatAntrianListModels.get(i).getJam_buka());
        startActivity(new Intent(this, AntrianActivity.class));
        finish();
    }
}