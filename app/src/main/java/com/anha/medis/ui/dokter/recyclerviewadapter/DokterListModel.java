package com.anha.medis.ui.dokter.recyclerviewadapter;

public class DokterListModel {
    private String dokter;
    private String hari;
    private String jam_buka;
    private String jam_tutup;
    private String kuota;

    public DokterListModel(String dokter, String hari, String jam_buka, String jam_tutup, String kuota) {
        this.dokter = dokter;
        this.hari = hari;
        this.jam_buka = jam_buka;
        this.jam_tutup = jam_tutup;
        this.kuota = kuota;
    }

    public String getDokter() {
        return dokter;
    }

    public void setDokter(String dokter) {
        this.dokter = dokter;
    }

    public String getHari() {
        return hari;
    }

    public void setHari(String hari) {
        this.hari = hari;
    }

    public String getJam_buka() {
        return jam_buka;
    }

    public void setJam_buka(String jam_buka) {
        this.jam_buka = jam_buka;
    }

    public String getJam_tutup() {
        return jam_tutup;
    }

    public void setJam_tutup(String jam_tutup) {
        this.jam_tutup = jam_tutup;
    }

    public String getKuota() {
        return kuota;
    }

    public void setKuota(String kuota) {
        this.kuota = kuota;
    }
}
