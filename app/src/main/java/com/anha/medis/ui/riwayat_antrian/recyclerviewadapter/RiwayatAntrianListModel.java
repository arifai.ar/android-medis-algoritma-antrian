package com.anha.medis.ui.riwayat_antrian.recyclerviewadapter;

public class RiwayatAntrianListModel {
    private String id;
    private String nomer;
    private String nama;
    private String BT;
    private String AT;
    private String tanggal;
    private String RT;
    private String TA;
    private String WT;
    private String jam_buka;

    public RiwayatAntrianListModel(String id, String nomer, String nama, String BT, String AT, String tanggal, String RT, String TA, String WT, String jam_buka) {
        this.id = id;
        this.nomer = nomer;
        this.nama = nama;
        this.BT = BT;
        this.AT = AT;
        this.tanggal = tanggal;
        this.RT = RT;
        this.TA = TA;
        this.WT = WT;
        this.jam_buka = jam_buka;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNomer() {
        return nomer;
    }

    public void setNomer(String nomer) {
        this.nomer = nomer;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getBT() {
        return BT;
    }

    public void setBT(String BT) {
        this.BT = BT;
    }

    public String getAT() {
        return AT;
    }

    public void setAT(String AT) {
        this.AT = AT;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getRT() {
        return RT;
    }

    public void setRT(String RT) {
        this.RT = RT;
    }

    public String getTA() {
        return TA;
    }

    public void setTA(String TA) {
        this.TA = TA;
    }

    public String getWT() {
        return WT;
    }

    public void setWT(String WT) {
        this.WT = WT;
    }

    public String getJam_buka() {
        return jam_buka;
    }

    public void setJam_buka(String jam_buka) {
        this.jam_buka = jam_buka;
    }
}
